// Copyright (c) 2018 Doyub Kim
//
// I am making my contributions/submissions to this project solely in my
// personal capacity and am not conveying any rights to any intellectual
// property of any third parties.

#include "../src.common/logging.h"
#include "manual_tests.h"
#include <fstream>
#include <gtest/gtest.h>

using namespace vox;

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  createDirectory(JET_TESTS_OUTPUT_DIR);

  std::ofstream logFile("manual_tests.log");
  if (logFile) {
    Logging::setAllStream(&logFile);
  }

  int ret = RUN_ALL_TESTS();

  return ret;
}
