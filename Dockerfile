FROM ubuntu:20.04

LABEL MAINTAINER FENG YANG
ENV REFRESHED_AT 2021-04-20

# install essential packages
RUN \
  mkdir -p /root/Pkg && \
  # use aliyun's mirror for better download speed
  sed -i 's/archive.ubuntu.com/mirrors.aliyun.com/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get install --no-install-recommends --no-install-suggests -y \
  make wget g++ libboost-dev libtbb-dev cmake

## copy code into docker
COPY ./ /root/Pkg 

## prepare Google log and tests
RUN \
cd /root/Pkg/external/glog-0.4.0 && \
mkdir build && \
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=/usr/local .. && \
make install -j4

RUN \
cd /root/Pkg/external/googletest-1.10.0 && \
mkdir build && \
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=/usr/local .. && \
make install -j4

## make unit_tests
RUN \
cd /root/Pkg/cmake-build-release && \ 
rm -rf ./* && \
cmake -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - Unix Makefiles" .. && \
cmake --build ./ --target unit_tests.flame -j4

## make template
RUN \
cd /root/Pkg/template && \
make clean && make

## run all tests
RUN \
cd /root/Pkg/unit_tests && \
../cmake-build-release/bin/unit_tests

CMD ["bash"]
